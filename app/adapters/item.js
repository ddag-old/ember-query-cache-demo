import DS from 'ember-data';

export default DS.RESTAdapter.extend({
	query: function(store, type, query) {
		console.log("made query", query);
		return {
			items: [{
				id: 1,
				name: "One"
			}, {
				id: 2,
				name: "Two"
			}]
		};
	}
});
